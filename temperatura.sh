#!/bin/bash

echo "*** Converting between the different temperature scales ***"
echo "1. Convert Celsius temperature into Fahrenheit"
echo "2. Convert Celsius temperature into Kelvin"
echo -n "Select your choice (F-K) : "
read choice

if [ $choice = F ]
then
  echo -n "Enter temperature (C) : "
  read tc
  # formula Tf=(9/5)*Tc+32
  tf=$(echo "scale=2;((9/5) * $tc) + 32" |bc)
  echo "$tc C = $tf F"
elif [ $choice = K ]
then
  echo -n "Enter temperature (C) : "
  read tc
  # formula Tk=Tc+273.15
  tk=$(echo "scale=2;($tc+273.15)"|bc)
  echo "$tc C = $tk K"
else
  echo "Please select F or K only"
  exit 1
fi


echo -n "
-->  Press any key to exit "
read echoice

exit 0
